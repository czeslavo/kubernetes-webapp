from flask import Flask
from flask_redis import FlaskRedis
from flask import jsonify
import os

app = Flask(__name__)
app.config.from_pyfile('config.cfg')
redis_store = FlaskRedis(app)

def get_or(r, key, default):
    try:
        return r.get('cookie_counter').decode('utf-8')
    except:
        return default

def get_cookies():
    return get_or(redis_store, 'cookie_counter', 0)

@app.route('/cookies', methods=['GET'])
def cookies_count():
    return jsonify(count=get_cookies())

@app.route('/produce', methods=['GET'])
def cookies_produce():
    redis_store.incr('cookie_counter')
    return jsonify(get_cookies())