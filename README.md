# Przygotowanie potrzebnych narzędzi

## IBM cloud CLI (ibmcloud)
https://clis.ng.bluemix.net/download/bluemix-cli/0.7.1/win64

## Kubernetes CLI (kubectl)
https://storage.googleapis.com/kubernetes-release/release/v1.10.3/bin/windows/amd64/kubectl.exe

## Helm 
https://storage.googleapis.com/kubernetes-helm/helm-v2.9.1-windows-amd64.zip
+ https://hub.kubeapps.com/

Wszystkie toole dodajemy gdzieś do PATHa. Można sprawdzić w PowerShellu:
`echo $env:PATH`

# Utworzenie klastra Kubernetes w IBM Cloud
1. Logujemy się do naszego dashboardu https://console.bluemix.net/dashboard/apps/
2. Rozwijamy menu po lewej i wybieramy `Containers`
3. Klikamy `Create a cluster`
4. Wybieramy opcję `Free`
5. Klaster zacznie się stawiać - możemy monitorować https://console.bluemix.net/containers-kubernetes/clusters

# Uzyskanie dostępu do klastra z poziomu kubectl
1. Logujemy się do IBM Cloud przez CLI 
```
ibmcloud login -a https://api.eu-gb.bluemix.net
```
2. Ustawiamy odpowiedni dla naszego klastra region
```
ibmcloud cs region-set uk-south
```
3. Pobieramy konfigurację naszego klastra
```
ibmcloud cs cluster-config mycluster
```
4. Ustawiamy zmienna środowiskową `KUBECONFIG`
```
$env:KUBECONFIG="/path/to/config"
```
5. Weryfikujemy dostęp do klastra 
```
kubectl get nodes
kubectl cluster-info
```

# Uzyskanie dostępu do dashboardu Kubernetes
Dostęp do dashboardu możemy uzyskać przez widok `Overview` naszego klastra w https://console.bluemix.net/containers-kubernetes/clusters. 

Innym sposobem jest użycie `kubectl proxy` - do tego celu jednak będziemy potrzebować tokena (Kubernetes używa RBAC).

# Stworzenie w klastrze deplymentu redisa
Użyjemy do tego celu menadżera pakietów `Helm`. Możemy przejrzeć repozytorium paczek https://hub.kubeapps.com/.

Aby Helm mógł współgrać z naszym klastrem, musimy wykonać polecenie `helm init`. Stworzy ono w klastrze agenta `Tiller`, z którym klient `helm` będzie się komunikował. 

```
helm install stable/redis --set master.persistence.enabled=false --name redis
kubectl get pods
kubectl get deployments
kubectl describe deployment redis-slave
kubectl get secrets
kubectl describe secret redis
```

# Frontend
Frontend bazuje na obrazie nginx - dodajemy swoją konfigurację, w której serwujemy statyczne pliki aplikacji w Angularze oraz ustawiamy proxy do backendowego serwisu, który utworzymy później. 

## Deployment
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: frontend-nginx
spec:
  selector:
    matchLabels:
      app: frontend-nginx
  template:
    metadata:
      labels:
        app: frontend-nginx
    spec:
      containers:
      - name: frontend-nginx
        image: czeslavo/frontend-nginx:latest
        ports:
        - containerPort: 80
```

## Service
```
kind: Service
apiVersion: v1
metadata:
  name: frontend-nginx
spec:
  selector:
    app: frontend-nginx
  ports:
  - port: 80
  type: NodePort
```

Żeby sprawdzić działanie, musimy uzyskać publiczny adres node'a z dashboardu IBM Cloud. Numer portu sprawdzimy `kubectl describe service frontend-nginx`. 

# API
API będzie łączyć się ze slave'ami redisa i czytać wartość licznika, udostępniając tę wartość na endpoincie `/cookies`. Musimy podmontować zmienną środowiskową z secretu redisa.

## Deployment
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: cookies-api
spec:
  selector:
    matchLabels:
      app: cookies-api
  template:
    metadata:
      labels:
        app: cookies-api
    spec:
      containers:
      - name: cookies-api
        image: czeslavo/cookies-api:latest
        ports:
        - containerPort: 8080
        env:          
        - name: REDIS_PASSWORD
          valueFrom:
            secretKeyRef:
              name: redis
              key: redis-password
```

## Service
```
kind: Service
apiVersion: v1
metadata:
  name: cookies-api
spec:
  selector:
    app: cookies-api
  ports:
  - port: 8080
    targetPort: 8080
```

# Bakery 
Piekarnia będzie komunikować się z masterem redisa i w nieskończonej pętli inkrementować licznik. 

## Deployment
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: bakery 
spec:
  replicas: 5
  selector:
    matchLabels:
      app: bakery
  template:
    metadata:
      labels:
        app: bakery 
    spec:
      containers:
      - name: bakery 
        image: czeslavo/bakery:latest
        env:
          - name: REDIS_PASSWORD
            valueFrom:
              secretKeyRef:
                name: redis
                key: redis-password
```