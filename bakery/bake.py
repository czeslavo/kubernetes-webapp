from redis import StrictRedis
from time import sleep
import os

master_host = os.environ.get('REDIS_MASTER_SERVICE_HOST', 'localhost')
master_port = os.environ.get('REDIS_MASTER_SERVICE_PORT', '6379')
password = os.environ.get('REDIS_PASSWORD', '')
r = StrictRedis(host=master_host, port=master_port, password=password)

def bake():
    while True:
        r.incr('cookie_counter')
        sleep(2)

if __name__ == '__main__':
    bake()