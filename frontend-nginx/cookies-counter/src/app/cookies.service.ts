import { Injectable } from '@angular/core';
import { environment } from '../environments/environment.prod';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CookiesService {
  constructor(private http: HttpClient) {}

  getApi() {
    return 'http://' + window.location.host + environment.api_url;
  }
  
  getCookies() {
    return this.http.get(this.getApi() + '/cookies');
  }
}
