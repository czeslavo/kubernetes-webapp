import { Component } from '@angular/core';
import { CookiesService } from './cookies.service';
import { Cookies } from './cookies';
import { timer } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private cookieService: CookiesService) {}

  cookies: number = 0;

  ngOnInit() {
    timer(1000, 1000).subscribe(
      ((x) => { this.updateCookies(); })
    );
  }

  updateCookies() {
    this.cookieService.getCookies()
      .subscribe((data: Cookies) => { 
        this.cookies = data['count']
      });
  }

}
